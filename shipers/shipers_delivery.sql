-- MySQL dump 10.13  Distrib 8.0.22, for Win64 (x86_64)
--
-- Host: localhost    Database: shipers
-- ------------------------------------------------------
-- Server version	8.0.22

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `delivery`
--

DROP TABLE IF EXISTS `delivery`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `delivery` (
  `id` int NOT NULL AUTO_INCREMENT,
  `address_reviced` varchar(255) DEFAULT NULL,
  `address_send` varchar(255) DEFAULT NULL,
  `carrier` varchar(255) DEFAULT NULL,
  `cost` int DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `mass` int DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `phone` int DEFAULT NULL,
  `photo` varchar(255) DEFAULT NULL,
  `recived` date DEFAULT NULL,
  `send` date DEFAULT NULL,
  `shipper` varchar(255) DEFAULT NULL,
  `statusd` varchar(255) DEFAULT NULL,
  `transport_cost` int DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `delivery`
--

LOCK TABLES `delivery` WRITE;
/*!40000 ALTER TABLE `delivery` DISABLE KEYS */;
INSERT INTO `delivery` VALUES (8,'hello3','faf','dadasd',122,'sda',12,'huong hao hoa',1231,'edsda','2020-10-09','2020-01-02','eawqewd','WAITING',100),(12,'236 hoàng quốc việt-từ liêm -hà nội','dadd','kjkkl',5654,'DDD',4,'phạm văn hưởng 123',357266402,'cvbnm','2020-08-10','2019-09-10','hieu','CONFIRM',1),(14,'236 hoàng quốc việt-từ liêm -hà nội','dadd','kjkkl',5654,'DDD',4,'phạm văn hưởng 123456',357266402,'cvbnm','2020-08-10','2019-09-10','hieu','CONFIRM',1),(19,'236 hoàng quốc việt-từ liêm -hà nội','hello3','carier huong',22,'DDDe',4,'phạm văn hưởng hào hoa',357266402,'cvbnm','2020-08-10','2019-09-10','bbljnl','CONFIRM',89),(20,'236 hoàng quốc việt-từ liêm -hà nội','hello','huong',22,'DDD',9,'phạm văn hưởng',357266402,'cvbnm','2020-08-10','2019-09-10','bbljnl','CONFIRM',89),(21,'236 hoàng quốc việt-từ liêm -hà nội','hello','huong',22,'DDD',9,'phạm văn hưởng',357266402,'cvbnm','2020-08-10','2019-09-10','bbljnl','CONFIRM',89),(22,'236 hoàng quốc việt-từ liêm -hà nội','hello','huong',22,'DDD',9,'phạm văn hưởng',357266402,'cvbnm','2020-08-10','2019-09-10','bbljnl','CONFIRM',89),(23,'236 hoàng quốc việt-từ liêm -hà nội','love you','carier huong',5654,'descrip',4,'phạm văn hưởng',357266402,'cvbnm','2020-08-10','2019-09-10','shipper','CONFIRM',3);
/*!40000 ALTER TABLE `delivery` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-02-24 17:45:07
