import React ,{Component} from 'react'
import axios from 'axios'
const URL="http://localhost:8088";
class DeliveryServices  {
    getdeliverys(){
       return  axios.get(URL);
    } 
    getdeliverysbyid(deliveryid){
        return axios.get(URL+'/'+deliveryid);
    }
    createdeliverys(delivery){
        return axios.post(""+URL,delivery);
    }
   
    updatedeliverys(delivery){
        return axios.put(URL+'/'+delivery.deliveryid,delivery);
    }
    deletedeliverys(deliveryid){
        return axios.delete(URL+'/'+deliveryid);
    }
}
export default new DeliveryServices();
