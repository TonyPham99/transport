import logo from './logo.svg';
import './App.css';
import ListDeliveryComponent from './Component/ListDeliveryComponent';
import FooterComponent from './Component/FooterComponent';
import HeaderComponent from './Component/HeaderComponent';
import { Route, BrowserRouter as Router, Switch } from 'react-router-dom';
import CreateComponent from './Component/CreateComponent';
import EditComponent from './Component/EditComponent';
function App() {
  return (
    <div>
    <Router>
      <HeaderComponent />
      <div className="container">



        <Switch>
             <Route path="/" exact component={ListDeliveryComponent}/>
             <Route path="/deliverys" component={ListDeliveryComponent}/>
             <Route path="/add-deliverys" component={CreateComponent}/>
             <Route path="/edit-deliverys" component={EditComponent}/>
            <ListDeliveryComponent/>
        </Switch>
        </div>


      <FooterComponent />
    </Router>
  </div>

  );
}

export default App;
