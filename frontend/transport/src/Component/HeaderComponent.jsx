import React ,{Component} from 'react'
class HeaderComponent extends React.Component {
    constructor(props) {
        super(props);

        this.state = {};

    }

    render() {
        return (
            <div>
                    <header className="header">
                        <nav className="navbar navbar-expand-md navbar-dark bg-dark ">
                            <div><a href="https://javaguides.net" className="navbar-brand">Delivery managerment App</a></div>
                        </nav>
                    </header>
            </div>
        );
    }
}

HeaderComponent.propTypes = {

};

export default HeaderComponent;