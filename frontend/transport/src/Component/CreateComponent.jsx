import React, { Component } from 'react';
import DeliveryServices from '../Services/DeliveryServices';

class CreateComponent extends Component {
    constructor(props) {
        super(props)
        this.state = {
            // id: this.props.match.params.id,
            name:'',
            addressReviced:'',
            addressSend:'',
            send:'',
            recived:'',
            carrier:'',
            description:'',
            mass:'',
            cost:'',
            transportCost:'',
            shipper: '',
            phone:'',
            photo:'',
            statusd:''

        }

        this.saveDelivery = this.saveDelivery.bind(this);
    }
   
    saveDelivery = (de) => {
        de.preventDefault();
        let delivery = {
            name: this.state.name,
            addressReviced: this.state.addressReviced,
            addressSend: this.state.addressSend,
            send: this.state.send,
            recived: this.state.recived,
            carrier: this.state.carrier,
            description: this.state.description,
            mass: this.state.mass,
            cost: this.state.cost,
            transportCost: this.state.transportCost,
            shipper: this.state.shipper,
            phone: this.state.phone,
            photo: this.state.photo,
            statusd: this.state.statusd

        };
        DeliveryServices.createdeliverys(delivery)
            .then (res=>{
                this.setState({message:'delivery added'});
                this.props.history.push("/deliverys");
            });


    }
    cancel() {
        this.props.history.push('/deliverys');
    }
    
    onChange=(de)=>this.setState({[de.target.name]:de.target.value});

    render() {
        return (
            <div>
                <div className="container">
                    <div className="row">
                        <div className="card col-md-6 offset-md-3 offset-md-3  " >
                            <div className="card col-md-6 offset-md-3 offset-md-3">
                               <h2>Add Deliverys</h2>
                            </div>
                            <div className="card-body">
                                <form className="formcreate">
                                    <div className="form-group">
                                        <label >Name Delivery</label>
                                        <input placeholder="Name delivery" name="name"
                                            className="form-control" value={this.state.name}
                                            onChange={this.onChange} />
                                    </div>
                                    <div className="form-group">
                                        <label >addressReviced</label>
                                        <input placeholder="Address delivery" name="addressReviced"
                                            className="form-control" value={this.state.addressReviced}
                                            onChange={this.onChange} />
                                    </div>
                                    <div className="form-group">
                                        <label >addressSend</label>
                                        <input placeholder="Address send delivery" name="addressSend"
                                            className="form-control" value={this.state.addressSend}
                                            onChange={this.onChange} />
                                    </div>

                                    <div className="form-group">
                                        <label >send</label>
                                        <input placeholder="send delivery" name="send"
                                            className="form-control" value={this.state.send}
                                            onChange={this.onChange} />
                                    </div>
                                    <div className="form-group">
                                        <label >recived</label>
                                        <input placeholder="recived delivery" name="recived"
                                            className="form-control" value={this.state.recived}
                                            onChange={this.onChange} />
                                    </div>
                                    <div className="form-group">
                                        <label >carrier</label>
                                        <input placeholder="carrier delivery" name="carrier"
                                            className="form-control" value={this.state.carrier}
                                            onChange={this.onChange} />
                                    </div>

                                    <div className="form-group">
                                        <label >description</label>
                                        <input placeholder="description delivery" name="description"
                                            className="form-control" value={this.state.description}
                                            onChange={this.onChange} />
                                    </div>
                                    <div className="form-group">
                                        <label >mass</label>
                                        <input placeholder="mass delivery" name="mass"
                                            className="form-control" value={this.state.mass}
                                            onChange={this.onChange} />
                                    </div>
                                    <div className="form-group">
                                        <label >cost</label>
                                        <input placeholder="cost delivery" name="cost"
                                            className="form-control" value={this.state.cost}
                                            onChange={this.onChange} />
                                    </div>

                                    <div className="form-group">
                                        <label >transportCost</label>
                                        <input placeholder="transportCost delivery" name="transportCost"
                                            className="form-control" value={this.state.transportCost}
                                            onChange={this.onChange} />
                                    </div>
                                    <div className="form-group">
                                        <label >shipper</label>
                                        <input placeholder="shipper delivery" name="shipper"
                                            className="form-control" value={this.state.shipper}
                                            onChange={this.onChange} />
                                    </div>

                                    <div className="form-group">
                                        <label >phone</label>
                                        <input placeholder="phone delivery" name="phone"
                                            className="form-control" value={this.state.phone}
                                            onChange={this.onChange} />
                                    </div>
                                    <div className="form-group">
                                        <label >photo</label>
                                        <input placeholder="photo delivery" name="photo"
                                            className="form-control" value={this.state.photo}
                                            onChange={this.onChange} />
                                    </div>
                                    <div className="form-group">
                                        <label >statusd</label>
                                        <input placeholder="statusd delivery" name="statusd"
                                            className="form-control" value={this.state.statusd}
                                            onChange={this.onChange} />
                                    </div>
                                    <button className="btn btn-success" onClick={this.saveDelivery}> Save</button>
                                    <button className="btn btn-danger" onClick={this.cancel.bind(this)} style={{ marginLeft: "10px" }}> Cancel</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default CreateComponent;