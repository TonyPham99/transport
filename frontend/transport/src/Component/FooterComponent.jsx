import React ,{Component} from 'react'
class FooterComponent extends React.Component {
    constructor(props) {
        super(props);

        this.state = {};

    }

    render() {
        return (
            <div>
                <footer className="footer text-center p-3">
                    <span className="text-dark">All rights reserved 2020 @Delivery</span>
                </footer>
            </div>
        );
    }
}

FooterComponent.propTypes = {

};

export default FooterComponent;