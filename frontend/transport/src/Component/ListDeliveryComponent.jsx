import React, { Component } from 'react'
import DeliveryServices from '../Services/DeliveryServices';
class ListDeliveryComponent extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            deliverys: [],
            message:null
        }
        this.addDelivery=this.addDelivery.bind(this);
        this.editDelivery=this.editDelivery.bind(this);
        this.deleteDelverys=this.deleteDelverys.bind(this);
        this.reloadDeliveryList=this.reloadDeliveryList.bind(this);

    }
    reloadDeliveryList(){
        DeliveryServices.getdeliverys().then((res) => {
            this.setState({ deliverys: res.data.data });
        });
    }
    deleteDelverys(deliveryid){
        DeliveryServices.deletedeliverys(deliveryid)
        .then (res=>{
            this.setState({message:'Delivery delete successffull'});
            this.setState({deliverys:this.state.deliverys.filter(de => de.id !== deliveryid)});

        })
    }
    // deleteDelverys(de){
    //    let deliverys=this.props.deliverys.filter(de =>de.id!==deliveryid)
    //    this.setState({deliverys});
    // }
    componentDidMount() {
        this.reloadDeliveryList();
    }
    
    addDelivery(){
        window.localStorage.removeItem("deliveryid");
        this.props.history.push('/add-deliverys');
        
    }
    editDelivery(id){
        window.localStorage.setItem("deliveryid",id)
        this.props.history.push('/edit-deliverys');
        
    }
    render() {
        return (
            <div>
                <h2 className="text-center">Delivery list</h2>
                <div className="row">
                    <button className="btn btn-primary" onClick={this.addDelivery}>Add Deliverys</button>
                </div>
                <div className="row">

                    <table className="table table-striped table-bordered">
                    <thead>
                        <tr>
                       
                        <th>name</th>
                        <th>addressReviced</th>
                        <th>addressSend</th>
                        <th>send</th>
                        <th>recived</th>
                        <th>carrier</th>
                        <th>description</th>
                        <th>mass</th>
                        <th>cost</th>
                        <th>transportCost</th>
                        <th>shipper</th>
                        <th>phone</th>
                        <th>photo</th>
                        <th>statusd</th>
                        <th>actions</th>
                        </tr>
                    </thead>
                        <tbody>
                            {

                                this.state.deliverys.map(
                                    de=>
                                        <tr key={de.id}>
                                            <td>{de.name}</td>
                                            <td>{de.addressReviced}</td>
                                            <td>{de.addressSend}</td>
                                            <td>{de.send}</td>
                                            <td>{de.recived}</td>
                                            <td>{de.carrier}</td>
                                            <td>{de.description}</td>
                                            <td>{de.mass}</td>
                                            <td>{de.cost}</td>
                                            <td>{de.transportCost}</td>
                                            <td>{de.shipper}</td>
                                            <td>{de.phone}</td>
                                            <td>{de.photo}</td>
                                            <td>{de.statusd}</td>
                                            <td>
                                                <button onClick={() => this.editDelivery(de.id)} className="btn btn-info">Update</button>
                                                <button style={{ marginLeft: "10px" }} onClick={() => this.deleteDelverys(de.id)} className="btn btn-danger">Delete</button>

                                            </td>
                                        </tr>
                                )
                            }

                        </tbody>
                    </table>
                </div>
            </div>
        );
    }
}
export default ListDeliveryComponent;