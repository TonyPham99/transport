package hdqtech.main.controllers;

import hdqtech.main.entity.Delivery;
import hdqtech.main.services.DeliveryServices;
import hdqtech.main.services.ServiceResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
@CrossOrigin("http://localhost:3000")
@RestController

public class DeliveryController {
    @Autowired
    private DeliveryServices deliveryServices;

    // getAll delivery
    @GetMapping("/")
    public ResponseEntity<ServiceResult> getAllDelivery() {
        return new ResponseEntity<ServiceResult>(deliveryServices.getAll(), HttpStatus.OK);

    }

    //     get All Delivery By Id
    @GetMapping("/{id}")
    public ResponseEntity<ServiceResult> getAllDeliveryById(@PathVariable int id) {
        return new ResponseEntity<ServiceResult>(deliveryServices.findById(id), HttpStatus.OK);
    }

    //     create delivery
    @PostMapping("/")
    public ResponseEntity<ServiceResult> createDelivery(@RequestBody Delivery delivery) {
        return new ResponseEntity<ServiceResult>(deliveryServices.createDelivery(delivery), HttpStatus.OK);

    }

    //update delivery
    @PutMapping("/{id}")
    public ResponseEntity<ServiceResult> updateDelivery(@RequestBody Delivery delivery) {
        return new ResponseEntity<ServiceResult>(deliveryServices.updateDelivery(delivery), HttpStatus.OK);

    }

    // delete Delivery
    @DeleteMapping("/{id}")
    public ResponseEntity<ServiceResult> deleteDelivery(@PathVariable int id) {
        return new ResponseEntity<ServiceResult>(deliveryServices.deleteDelivery(id), HttpStatus.OK);
    }
}
