package transport.mainauth;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TransportEurekaAuthApplication {

	public static void main(String[] args) {
		SpringApplication.run(TransportEurekaAuthApplication.class, args);
	}

}
